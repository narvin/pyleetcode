"""Tests for solutions to https://leetcode.com/problems/3sum/"""
from typing import List

import pytest

from leetcode.p0015_3sum import Solution


class TestSolution:
    """Solutions test suite."""

    @staticmethod
    @pytest.mark.parametrize(
        "nums, expected",
        [
            ([-1, 0, 1, 2, -1, -4], [[-1, -1, 2], [-1, 0, 1]]),
            ([0, 1, 1], []),
            ([0, 0, 0], [[0, 0, 0]]),
        ],
    )
    def test_three_sum(nums: List[int], expected: List[List[int]]) -> None:
        """Tests for the three_sum function."""
        soln = Solution()
        assert sorted(soln.three_sum(nums)) == sorted(expected)
