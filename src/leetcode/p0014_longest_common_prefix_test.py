"""Tests for solutions to https://leetcode.com/problems/longest-common-prefix/"""
from typing import List

import pytest

from leetcode.p0014_longest_common_prefix import Solution


class TestSolution:
    """Solutions test suite."""

    TESTS = [
        (["flower", "flow", "flight"], "fl"),
        (["dog", "racecar", "car"], ""),
        (["ab", "a"], "a"),
    ]

    @staticmethod
    @pytest.mark.parametrize("strs, expected", TESTS)
    def test_horizontal_soln(strs: List[str], expected: str) -> None:
        """Tests for the two_sum function."""
        soln = Solution()
        assert soln.longest_common_prefix_horizontal_soln(strs) == expected

    @staticmethod
    @pytest.mark.parametrize("strs, expected", TESTS)
    def test_vertical_soln(strs: List[str], expected: str) -> None:
        """Tests for the two_sum function."""
        soln = Solution()
        assert soln.longest_common_prefix_vertical_soln(strs) == expected

    @staticmethod
    @pytest.mark.parametrize("strs, expected", TESTS)
    def test_recursive_soln(strs: List[str], expected: str) -> None:
        """Tests for the two_sum function."""
        soln = Solution()
        assert soln.longest_common_prefix_recursive_soln(strs) == expected
