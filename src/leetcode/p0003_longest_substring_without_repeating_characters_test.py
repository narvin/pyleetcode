"""
Tests for solutions to
https://leetcode.com/problems/longest-substring-without-repeating-characters/
"""
import pytest

from leetcode.p0003_longest_substring_without_repeating_characters import Solution


class TestSolution:
    """Solutions test suite."""

    @staticmethod
    @pytest.mark.parametrize(
        "string, exp_len",
        [
            ("pwwkew", 3),
            ("abcabcbb", 3),
            ("bbbbb", 1),
            ("tmmzuxt", 5),
        ],
    )
    def test_length_of_longest_substring(string: str, exp_len: int) -> None:
        """Tests for the length_of_longest_substring function."""
        soln = Solution()
        assert soln.length_of_longest_substring(string) == exp_len
