"""Solutions to https://leetcode.com/problems/two-sum/"""
from typing import Dict, List


class Solution:
    """Two Sum solutions"""

    def two_sum(self, nums: List[int], target: int) -> List[int]:
        """
        Hash table to lookup previously encountered nums in O(1)
        Time: O(n)
        Space: O(n)
        """
        num_memory: Dict[int, int] = {}
        for idx, num in enumerate(nums):
            other_num_idx = num_memory.get(target - num)
            if other_num_idx is not None:
                return [other_num_idx, idx]
            num_memory[num] = idx
        return []
