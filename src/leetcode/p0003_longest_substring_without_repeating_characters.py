"""
Solutions to
https://leetcode.com/problems/longest-substring-without-repeating-characters/
"""
from typing import Dict


class Solution:
    """Longest Substring Without Repeating Characters solutions"""

    def length_of_longest_substring(self, string: str) -> int:
        """
        Hash table to keep track of indices of encountered characters
        Time: O(n) where n is the length of the string
        Space: O(m) where m is the number of distinct characters in the string
        """
        char_memory: Dict[str, int] = {}
        max_substr_len, left_idx = 0, 0
        for idx, char in enumerate(string):
            if char in char_memory:
                # If the left index is not past the previous occurence of char,
                # then move it just after the previous occurence of char
                left_idx = max(char_memory[char] + 1, left_idx)
            char_memory[char] = idx
            max_substr_len = max(idx - left_idx + 1, max_substr_len)
        return max_substr_len
