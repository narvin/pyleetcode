"""Tests for solutions to https://leetcode.com/problems/add-two-numbers/"""
from typing import List, Optional

import pytest

from leetcode.p0002_add_two_numbers import ListNode, Solution


class TestSolution:
    """Solutions test suite."""

    @staticmethod
    @pytest.mark.parametrize(
        "ls1, ls2, exp_sum",
        [
            ([2, 4, 3], [5, 6, 4], [7, 0, 8]),
            ([0], [0], [0]),
            ([9, 9, 9, 9, 9, 9, 9], [9, 9, 9, 9], [8, 9, 9, 9, 0, 0, 0, 1]),
        ],
    )
    def test_add_two_numbers(
        ls1: List[int], ls2: List[int], exp_sum: List[int]
    ) -> None:
        """Tests for the add_two_numbers function."""

        def create_list(nums: List[int]) -> Optional[ListNode]:
            pre_head = ListNode()
            curr = pre_head
            for num in nums:
                curr.next = ListNode(num)
                curr = curr.next
            return pre_head.next

        soln = Solution()
        act_sum_ls = soln.add_two_numbers(create_list(ls1), create_list(ls2))
        exp_sum_ls = create_list(exp_sum)
        while act_sum_ls and exp_sum_ls:
            print(f"{act_sum_ls.val} {exp_sum_ls.val}")
            assert act_sum_ls.val == exp_sum_ls.val
            act_sum_ls, exp_sum_ls = act_sum_ls.next, exp_sum_ls.next
        assert act_sum_ls is None
        assert exp_sum_ls is None
