PYPATH := $(dir $(abspath $(lastword $(MAKEFILE_LIST))))
FORMAT := PYTHONPATH=$(PYPATH) black
LINT := PYTHONPATH=$(PYPATH) pylint --exit-zero -s n
STYLE := PYTHONPATH=$(PYPATH) pycodestyle
TYPE := PYTHONPATH=$(PYPATH) mypy --no-error-summary
TEST := PYTHONPATH=$(PYPATH) pytest -n auto
TAGS := ctags -R

.PHONY: all format lint style type test tags clean

all: lint style type

format:
	@printf '===== FORMAT =====\n'
	@$(FORMAT) src

lint:
	@printf '===== LINT =====\n'
	@$(LINT) src

style:
	@printf '===== STYLE =====\n'
	@$(STYLE) src || true

type:
	@printf '===== TYPE =====\n'
	@$(TYPE) src || true

test:
	@printf '===== TEST =====\n'
	@$(TEST) src

tags:
	@printf '===== TAGS =====\n'
	@$(TAGS) src .venv/lib/python3.10/site-packages

clean:
	find src -type d -name __pycache__ -exec rm -rf {} \;
