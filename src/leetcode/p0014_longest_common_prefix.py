"""Solutions to https://leetcode.com/problems/longest-common-prefix/"""
from typing import List


class Solution:
    """Longest Common Prefix solutions"""

    def longest_common_prefix_horizontal_soln(self, strs: List[str]) -> str:
        """
        Iterate through list keeping, track of longest prefix
        Time: O(n*k), where n = len(strs) and k is the length of the longest word
        Space: O(1)
        """
        lcp = strs[0]
        for word in strs[1:]:
            clamped_len = min(len(word), len(lcp))
            lcp = lcp[:clamped_len]
            for i in range(clamped_len):
                if word[i] != lcp[i]:
                    lcp = word[:i]
                    break
        return lcp

    def longest_common_prefix_vertical_soln(self, strs: List[str]) -> str:
        """
        Iterate through each letter of first word, then each letter at that position
        for the rest of the words in the list
        Time: O(k*n), where k is the length of the longest word, and n = len(strs)
        Space: O(1)
        """
        lcp = strs[0]
        for col_idx in range(len(lcp)):
            for row_idx in range(1, len(strs)):
                if (
                    col_idx == len(strs[row_idx])
                    or strs[row_idx - 1][col_idx] != strs[row_idx][col_idx]
                ):
                    return lcp[:col_idx]
        return lcp

    def longest_common_prefix_recursive_soln(self, strs: List[str]) -> str:
        """
        Return the lcp of the left and right halves of the list. The base case is
        a list of 1 word, and the lcp is that word. Recursively divide each half of
        the list until the base case is reached, and keep returning the lcp of each
        half, which will be the lcp of two words.
        Time: O(n*k), where n = len(strs) and k is the length of the longest word
        Space: O(1)
        """

        def lcp_of_two_strs(str1: str, str2: str) -> str:
            lcp = ""
            for c_of_1, c_of_2 in zip(str1, str2):
                if c_of_1 != c_of_2:
                    return lcp
                lcp += c_of_1
            return lcp

        def recurse(left: int, right: int) -> str:
            if left == right:
                return strs[left]
            mid = (left + right) // 2  # left + (right - left) // 2
            left_lcp = recurse(left, mid)
            right_lcp = recurse(mid + 1, right)
            return lcp_of_two_strs(left_lcp, right_lcp)

        return recurse(0, len(strs) - 1)
