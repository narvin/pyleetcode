"""Tests for solutions to https://leetcode.com/problems/two-sum/"""
from typing import List

import pytest

from leetcode.p0001_two_sum import Solution


class TestSolution:
    """Solutions test suite."""

    @staticmethod
    @pytest.mark.parametrize(
        "nums, target, expected",
        [
            ([2, 7, 11, 15], 9, [0, 1]),
            ([3, 2, 4], 6, [1, 2]),
            ([3, 3], 6, [0, 1]),
        ],
    )
    def test_two_sum(nums: List[int], target: int, expected: List[int]) -> None:
        """Tests for the two_sum function."""
        soln = Solution()
        assert soln.two_sum(nums, target) == expected
