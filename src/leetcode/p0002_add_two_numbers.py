"""Solutions to https://leetcode.com/problems/add-two-numbers"""
from typing import Optional


class ListNode:
    """A linked list node"""

    def __init__(self, val: int = 0, nxt: Optional["ListNode"] = None):
        self.val = val
        self.next = nxt


class Solution:
    """Add Two Numbers solutions"""

    def add_two_numbers(
        self, ls1: Optional[ListNode], ls2: Optional[ListNode]
    ) -> Optional[ListNode]:
        """
        Iterate through each list, adding the digits for that place, and keeping
        track of the carry for the next place.
        Time: O(n) where n is the length of the longest list
        Space: O(n)
        """
        pre_head = ListNode()
        curr_place = pre_head
        carry = 0
        while ls1 and ls2:
            place_sum = ls1.val + ls2.val + carry
            place_digit, carry = place_sum % 10, place_sum // 10
            curr_place.next = ListNode(place_digit)
            ls1, ls2, curr_place = ls1.next, ls2.next, curr_place.next

        ls12 = ls1 if ls1 else ls2
        while ls12 and carry:
            place_sum = ls12.val + carry
            place_digit, carry = place_sum % 10, place_sum // 10
            curr_place.next = ListNode(place_digit)
            ls12, curr_place = ls12.next, curr_place.next

        if carry:
            curr_place.next = ListNode(carry)
            curr_place = curr_place.next

        curr_place.next = ls12
        return pre_head.next
