"""Solutions to https://leetcode.com/problems/3sum/"""
from typing import List


class Solution:
    """3Sum solutions"""

    def three_sum(self, nums: List[int]) -> List[List[int]]:
        """
        Sliding window on sorted list. For each num in nums, use the sliding window
        to find two numbers to the right of num s.t. all three will sum to zero.
        Time: O(nlogn + n^2) -> O(n2)
        Space: O(1) or O(n) if we can't sort nums in-place
        """
        trips: List[List[int]] = []
        nums.sort()
        for idx, num in enumerate(nums):
            if idx > 0 and num == nums[idx - 1]:
                continue
            left, right = idx + 1, len(nums) - 1
            while left < right:
                trip_sum = num + nums[left] + nums[right]
                if trip_sum == 0:
                    trips.append([num, nums[left], nums[right]])
                    prev_left_num = nums[left]
                    left += 1
                    while left < right and nums[left] == prev_left_num:
                        left += 1
                if trip_sum < 0:
                    left += 1
                else:
                    right -= 1
        return trips
